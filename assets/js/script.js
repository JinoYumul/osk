let output = document.querySelector("#output");

function typeContent(e){
	let content = e.target.innerHTML;
	output.value += content;
};

function clearAll(){
	output.value = "";
}

function backSpace(){
	output.value = output.value.slice(0, -1);
}

function spaceBar(){
	output.value += " ";
}

function enterKey(){
	output.value += "\n";
}

let input = document.getElementsByClassName("input");
for(let i = 0; i < input.length; i++){
  input[i].addEventListener("click", typeContent);
}

let clearall = document.querySelector("#clearAll");
// console.log(clearall)
clearall.addEventListener("click", clearAll);

let backspace = document.querySelector("#backspace");
backspace.addEventListener("click", backSpace);

let enter = document.querySelector("#enter");
enter.addEventListener("click", enterKey);

let space = document.querySelector("#space");
space.addEventListener("click", spaceBar);